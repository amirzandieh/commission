<?php

namespace Tests\Feature;

use App\Services\CommissionCalculator\CommissionCalculatorInterface;
use Tests\TestCase;

class CommissionCalculatorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_flow_of_commission_calculator_service()
    {
        $commissionCalculator = app()->make(CommissionCalculatorInterface::class);
        $file = public_path() . '/CSV/input-1.csv';

        $response = $commissionCalculator->handle($file);

        $result = "";
        foreach ($response as $item) {
            $result .= $item['result'] . " ";
        }

        $this->assertEquals(trim($result), "0.60 3.00 0.00 0.06 1.50 0 0.70 0.30 0.30 3.00 0.00 0.00 8612");
    }
}
