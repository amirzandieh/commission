<?php

namespace App\Services\DataProvider;

use App\Enums\InputArrayKeys;

class DataProviderService extends AbstractDataProvider implements DataProviderInterface
{
    /** @var DataProviderFactory */
    private DataProviderFactory $dataProviderFactory;

    /**
     * @param DataProviderFactory $dataProviderFactory
     */
    public function __construct(DataProviderFactory $dataProviderFactory)
    {
        $this->dataProviderFactory = $dataProviderFactory;
    }

    /**
     * @param string $input
     *
     * @return array
     */
    public function provide(string $input): array
    {
        $provider = $this->dataProviderFactory->make($input);

        $records = $provider->execute($input);

        $records = $this->sortIndexing($records);

        return $records;
    }

    /**
     * @param array $records
     *
     * @return array
     */
    public function sortIndexing(array $records): array
    {
        $recordsAccordingUsers = [];

        foreach ($records as $index => $record) {
            $record[InputArrayKeys::SORT_INDEX] = $index;
            $userId = $record[InputArrayKeys::USER_ID];
            $usertype = $record[InputArrayKeys::USER_TYPE];
            $operationType = $record[InputArrayKeys::OPERATION_TYPE];
            $recordsAccordingUsers [$userId . '-' . $usertype . '-' . $operationType][] = $record;
        }

        return $recordsAccordingUsers;
    }
}
