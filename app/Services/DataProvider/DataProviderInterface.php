<?php

namespace App\Services\DataProvider;

interface DataProviderInterface
{
    /**
     * @param string $input
     *
     * @return array
     */
    public function provide(string $input): array;
}
