<?php

namespace App\Services\DataProvider;

use App\Services\DataProvider\Providers\FileProviderInterface;
use Nette\FileNotFoundException;

class DataProviderFactory
{
    /**
     * @param string $input
     *
     * @return FileProviderInterface
     */
    public function make(string $input): FileProviderInterface
    {
        $ext = pathinfo($input, PATHINFO_EXTENSION);
        try {
            return app()->make('App\Services\DataProvider\Providers\\' . strtoupper($ext) . 'Provider' );
        } catch (\Exception $exception) {
            throw new FileNotFoundException('You should implement this type of provider, {' . strtoupper($ext) . 'Provider' .'}');
        }
    }
}
