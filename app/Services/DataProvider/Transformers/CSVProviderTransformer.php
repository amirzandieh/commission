<?php

namespace App\Services\DataProvider\Transformers;

use App\Enums\InputArrayKeys;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CSVProviderTransformer extends JsonResource implements DataProviderTransformerInterface
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            InputArrayKeys::OPERATION_DATE => $this->getOperationDate(),
            InputArrayKeys::USER_ID => $this->getUserId(),
            InputArrayKeys::USER_TYPE => $this->getUserType(),
            InputArrayKeys::OPERATION_TYPE => $this->getOperationType(),
            InputArrayKeys::OPERATION_AMOUNT => $this->getOperationAmount(),
            InputArrayKeys::OPERATION_CURRENCY => $this->getOperationCurrency(),
        ];
    }

    /**
     * @return string
     */
    public function getOperationDate(): string
    {
        return $this->resource[InputArrayKeys::OPERATION_DATE] ?? '';
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->resource[InputArrayKeys::USER_ID] ?? '';
    }

    /**
     * @return string
     */
    public function getUserType(): string
    {
        return $this->resource[InputArrayKeys::USER_TYPE] ?? '';
    }

    /**
     * @return string
     */
    public function getOperationType(): string
    {
        return $this->resource[InputArrayKeys::OPERATION_TYPE] ?? '';
    }

    /**
     * @return string
     */
    public function getOperationAmount(): string
    {
        return $this->resource[InputArrayKeys::OPERATION_AMOUNT] ?? '';
    }

    /**
     * @return string
     */
    public function getOperationCurrency(): string
    {
        return $this->resource[InputArrayKeys::OPERATION_CURRENCY] ?? '';
    }
}
