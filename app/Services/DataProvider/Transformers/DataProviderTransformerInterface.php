<?php

namespace App\Services\DataProvider\Transformers;

use App\Enums\InputArrayKeys;

interface DataProviderTransformerInterface
{
    /**
     * @return string
     */
    public function getOperationDate(): string;

    /**
     * @return string
     */
    public function getUserId(): string;

    /**
     * @return string
     */
    public function getUserType(): string;

    /**
     * @return string
     */
    public function getOperationType(): string;

    /**
     * @return string
     */
    public function getOperationAmount(): string;

    /**
     * @return string
     */
    public function getOperationCurrency(): string;
}
