<?php

namespace App\Services\DataProvider\Providers;

use App\Services\DataProvider\Transformers\CSVProviderTransformer;

class CSVProvider implements FileProviderInterface
{
    /**
     * @param string $file
     * @return array
     */
    public function execute(string $file): array
    {
        $records = csvToArray($file);

        return CSVProviderTransformer::collection($records)->toArray(request());
    }
}
