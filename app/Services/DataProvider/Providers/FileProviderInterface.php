<?php

namespace App\Services\DataProvider\Providers;

interface FileProviderInterface
{
    /**
     * @param string $file
     * @return array
     */
    public function execute(string $file): array;
}
