<?php

namespace App\Services\CommissionCalculator;

use App\Services\CommissionCalculator\Strategies\CommissionCalculatorStrategyDecider;
use App\Services\DataProvider\DataProviderInterface;

class CommissionCalculatorService implements CommissionCalculatorInterface
{
    /** @var DataProviderInterface  */
    private DataProviderInterface $dataProvider;

    /** @var CommissionCalculatorStrategyDecider  */
    private CommissionCalculatorStrategyDecider $commissionStrategy;

    /**
     * @param DataProviderInterface $dataProvider
     */
    public function __construct(DataProviderInterface $dataProvider, CommissionCalculatorStrategyDecider $commissionStrategy)
    {
        $this->dataProvider = $dataProvider;
        $this->commissionStrategy = $commissionStrategy;
    }

    /**
     * @param string $input
     *
     * @return array
     */
    public function handle(string $input): array
    {
        $records = $this->dataProvider->provide($input);

        $response = [];
        foreach ($records as $key => $record)
        {
            $recordType = explode('-', $key);
            $usertype = $recordType[1];
            $operationType = $recordType[2];

            $result = $this->commissionStrategy->decide($operationType, $usertype, $record);
            $response = array_merge($response, $result);
        }

        $response = collect($response)->sortBy('sort_index')->toArray();

        return $response;
    }
}
