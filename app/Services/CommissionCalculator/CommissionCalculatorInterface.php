<?php

namespace App\Services\CommissionCalculator;

interface CommissionCalculatorInterface
{
    /**
     * @param string $input
     *
     * @return array
     */
    public function handle(string $input): array;
}
