<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes;

use App\Enums\CommissionRules;
use App\Enums\CurrencyRules;
use App\Enums\InputArrayKeys;

class UserPrivateType implements UserTypeInterface
{
    /**
     * @param array $records
     *
     * @return array
     */
    public function execute(array $records): array
    {
        $recordsAccordingDate = [];
        foreach ($records as $record) {
            $date = $record[InputArrayKeys::OPERATION_DATE];
            $year = date("Y", strtotime($date));
            $week = date("W", strtotime($date));
            $day = date("d", strtotime($date));
            $month = date("m", strtotime($date));

            if ($day == 31 && $month == 12) {
                $year = (string) ((int) $year + 1);
            }

            $recordsAccordingDate[$year . '-' . $week][] = $record;
        }

        $recordsAccordingDate = array_values($recordsAccordingDate);

        $result = [];
        foreach ($recordsAccordingDate as $key => $recordsDate)
        {
            $totalFree = 1000;
            foreach ($recordsDate as $recordDate) {
                $amount = convertCurrencyToEUR((string) $recordDate[InputArrayKeys::OPERATION_AMOUNT], $recordDate[InputArrayKeys::OPERATION_CURRENCY]);
                if ($totalFree > 0 && $key < 3)
                {
                    if ($totalFree >= $amount) {
                        $totalFree = $totalFree - $amount;
                        $remaining = 0;
                    } else {
                        $remaining = $amount - $totalFree;
                        $totalFree = 0;
                    }

                    $amountResult = $remaining * CommissionRules::PRIVATE_WITHDRAW / 100;
                } else {
                    $amountResult = $amount * CommissionRules::PRIVATE_WITHDRAW / 100;
                }

                $precision = strlen(substr(strrchr($recordDate[InputArrayKeys::OPERATION_AMOUNT], "."), 1));
                $value = $amountResult * CurrencyRules::getConstant('EUR_' . $recordDate[InputArrayKeys::OPERATION_CURRENCY]);

                if ($precision == 0) {
                    $value = ceil($value);
                }

                $result[] = [
                    'result' => number_format(roundUp((string) $value, (string) $precision), $precision, '.', ''),
                    'sort_index' => $recordDate[InputArrayKeys::SORT_INDEX]
                ];
            }
        }

        return $result;
    }
}
