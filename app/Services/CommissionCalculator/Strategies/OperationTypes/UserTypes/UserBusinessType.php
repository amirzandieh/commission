<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes;

use App\Enums\CommissionRules;
use App\Enums\InputArrayKeys;

class UserBusinessType implements UserTypeInterface
{
    /**
     * @param array $records
     *
     * @return array
     */
    public function execute(array $records): array
    {
        $result = [];
        foreach ($records as $record)
        {
            $amount = convertCurrencyToEUR((string) $record[InputArrayKeys::OPERATION_AMOUNT], $record[InputArrayKeys::OPERATION_CURRENCY]);

            $precision = strlen(substr(strrchr($record[InputArrayKeys::OPERATION_AMOUNT], "."), 1));
            $value = $amount * CommissionRules::BUSINESS_WITHDRAW / 100;
            $result[] = [
                'result' => number_format(roundUp((string) $value, (string) $precision), $precision),
                'sort_index' => $record[InputArrayKeys::SORT_INDEX]
            ];
        }

        return $result;
    }
}
