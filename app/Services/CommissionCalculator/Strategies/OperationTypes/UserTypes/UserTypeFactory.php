<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes;

use Nette\FileNotFoundException;

class UserTypeFactory
{
    /**
     * @param string $userType
     *
     * @return UserTypeInterface
     */
    public function make(string $userType): UserTypeInterface
    {
        try {
            return app()->make('App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes\User' . ucfirst($userType) . 'Type');
        } catch (\Exception $exception) {
            throw new FileNotFoundException('You should implement this operationType, {' . 'User' . ucfirst($userType) . 'Type' .'}');
        }
    }
}
