<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes;

interface UserTypeInterface
{
    /**
     * @param array $record
     *
     * @return array
     */
    public function execute(array $record): array;
}
