<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes;

use Nette\FileNotFoundException;

class OperationTypeFactory
{
    /**
     * @param string $operationType
     *
     * @return OperationTypeInterface
     */
    public function make(string $operationType): OperationTypeInterface
    {
        try {
            return app()->make('App\Services\CommissionCalculator\Strategies\OperationTypes\\' . ucfirst($operationType) . 'Operation');
        } catch (\Exception $exception) {
            throw new FileNotFoundException('You should implement this operationType, {' . ucfirst($operationType) . 'Operation' .'}');
        }
    }
}
