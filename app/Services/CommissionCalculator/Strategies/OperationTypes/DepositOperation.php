<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes;

use App\Enums\CommissionRules;
use App\Enums\InputArrayKeys;

class DepositOperation implements OperationTypeInterface
{
    /**
     * @param string $userType
     * @param array $records
     *
     * @return array
     */
    public function execute(string $userType, array $records): array
    {
        $result = [];
        foreach ($records as $record)
        {
            $amount = convertCurrencyToEUR((string) $record[InputArrayKeys::OPERATION_AMOUNT], $record[InputArrayKeys::OPERATION_CURRENCY]);

            $precision = strlen(substr(strrchr($record[InputArrayKeys::OPERATION_AMOUNT], "."), 1));
            $value = $amount * CommissionRules::DEPOSIT / 100;
            $result[] = [
                'result' => number_format(roundUp((string) $value, (string) $precision), $precision),
                'sort_index' => $record[InputArrayKeys::SORT_INDEX]
            ];
        }

        return $result;
    }
}
