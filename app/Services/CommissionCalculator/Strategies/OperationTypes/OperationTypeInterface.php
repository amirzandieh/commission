<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes;

interface OperationTypeInterface
{
    /**
     * @param string $userType
     * @param array $records
     *
     * @return array
     */
    public function execute(string $userType, array $records): array;
}
