<?php

namespace App\Services\CommissionCalculator\Strategies\OperationTypes;

use App\Services\CommissionCalculator\Strategies\OperationTypes\UserTypes\UserTypeFactory;

class WithdrawOperation implements OperationTypeInterface
{
    /** @var UserTypeFactory */
    private UserTypeFactory $userTypeFactory;

    /**
     * @param UserTypeFactory $userTypeFactory
     */
    public function __construct(UserTypeFactory $userTypeFactory)
    {
        $this->userTypeFactory = $userTypeFactory;
    }

    /**
     * @param string $userType
     * @param array $records
     *
     * @return array
     */
    public function execute(string $userType, array $records): array
    {
        $userType = $this->userTypeFactory->make($userType);

        return $userType->execute($records);
    }
}
