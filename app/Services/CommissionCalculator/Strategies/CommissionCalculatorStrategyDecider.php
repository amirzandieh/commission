<?php

namespace App\Services\CommissionCalculator\Strategies;

use App\Services\CommissionCalculator\Strategies\OperationTypes\OperationTypeFactory;

class CommissionCalculatorStrategyDecider
{
    private OperationTypeFactory $operationTypeFactory;

    public function __construct(OperationTypeFactory $operationTypeFactory)
    {
        $this->operationTypeFactory = $operationTypeFactory;
    }

    public function decide(string $operationType, string $userType, array $records)
    {
        $operationType = $this->operationTypeFactory->make($operationType);

        return $operationType->execute($userType, $records);
    }
}
