<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class CurrencyRules extends Enum
{
    const EUR_USD = 1.1497;
    const EUR_JPY = 129.53;
    const EUR_EUR = 1;

    static function getConstant($constant)
    {
        return constant('self::'. $constant);
    }
}
