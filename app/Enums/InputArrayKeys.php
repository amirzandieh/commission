<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class InputArrayKeys extends Enum
{
    const OPERATION_DATE = 0;
    const USER_ID = 1;
    const USER_TYPE = 2;
    const OPERATION_TYPE = 3;
    const OPERATION_AMOUNT = 4;
    const OPERATION_CURRENCY = 5;

    const SORT_INDEX = 6;
}
