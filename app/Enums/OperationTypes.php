<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class OperationTypes extends Enum
{
    const DEPOSIT = 'deposit';
    const WITHDRAW = 'withdraw';
}
