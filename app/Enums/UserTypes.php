<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class UserTypes extends Enum
{
    const PRIVATE = 'private';
    const BUSINESS = 'business';
}
