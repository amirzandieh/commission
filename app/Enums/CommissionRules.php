<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class CommissionRules extends Enum
{
    const DEPOSIT = 0.03;
    const PRIVATE_WITHDRAW = 0.3;
    const BUSINESS_WITHDRAW = 0.5;
}
