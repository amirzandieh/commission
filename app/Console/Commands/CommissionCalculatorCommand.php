<?php

namespace App\Console\Commands;

use App\Services\CommissionCalculator\CommissionCalculatorInterface;
use Illuminate\Console\Command;

class CommissionCalculatorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:calculator {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commission Calculator';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(CommissionCalculatorInterface $commissionCalculator)
    {
        $file = $this->argument('file');

        $response = $commissionCalculator->handle($file);

        foreach ($response as $item) {
            echo $item['result'];
            echo "\r\n";
        }

        return 0;
    }
}
