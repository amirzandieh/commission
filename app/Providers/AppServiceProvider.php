<?php

namespace App\Providers;

use App\Services\CommissionCalculator\CommissionCalculatorInterface;
use App\Services\CommissionCalculator\CommissionCalculatorService;
use App\Services\DataProvider\DataProviderService;
use App\Services\DataProvider\DataProviderInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataProviderInterface::class, DataProviderService::class);
        $this->app->bind(CommissionCalculatorInterface::class, CommissionCalculatorService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
