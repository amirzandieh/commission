<?php

use App\Enums\CurrencyRules;

if (!function_exists('csvToArray')) {
    /**
     * @param string $csvFile
     *
     * @return array
     */
    function csvToArray(string $csvFile): array
    {
        $readFile = fopen($csvFile, 'r');

        $lines = [];
        while (!feof($readFile) ) {
            $lines[] = fgetcsv($readFile, 1000, ',');
        }

        fclose($readFile);
        return $lines;
    }
}

if (!function_exists('roundUp')) {
    /**
     * @param string $value
     * @param string $precision
     *
     * @return string
     */
    function roundUp (string $value, string $precision): string
    {
        $pow = pow (10, $precision);
        return (ceil ($pow * $value) + ceil ($pow * $value - ceil ($pow * $value))) / $pow;
    }
}

if (!function_exists('convertCurrencyToEUR')) {
    /**
     * @param string $amount
     * @param string $currency
     *
     * @return string
     */
    function convertCurrencyToEUR(string $amount, string $currency): string
    {
        $currencyRule = CurrencyRules::getConstant('EUR_' . $currency);

        return (string) ($amount / $currencyRule);
    }
}
